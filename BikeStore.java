// Tomas Daniel Nieto
// # 2034643
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bicycles = {
            new Bicycle("speed bike", 5, 10),
            new Bicycle("Slow bike", 3, 5),
            new Bicycle("Original bikes", 4, 4),
            new Bicycle("Bike manucturer", 8, 12.4),
        };

        for (Bicycle bicycle : bicycles) {
            System.out.println(bicycle);
        }
    }   
}

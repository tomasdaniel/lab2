// Tomas Daniel Nieto
// # 2034643
public class Bicycle {
	private String manufacturer;
	private int numberGear;
	private double maxSpeed;
	
	public Bicycle(String manufacturer, int numberGear, double maxSpeed) {
		this.manufacturer = manufacturer;
		this.numberGear = numberGear;
		this.maxSpeed = maxSpeed;
	}

	public String getManufacturer() {
		return manufacturer;
	}
	public int getNumberGear() {
		return numberGear;
	}
	public double getMaxSpeed() {
		return maxSpeed;
	}

	@Override
	public String toString() {
		return "manufacturer" + this.manufacturer + ", numberGear: " + this.numberGear + ", maxSpeed:" + maxSpeed;
	}
}